var React = require("react");
var api = require("./../utils/apis.js");

class UserInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    if (event.target.id === "email") {
      this.setState(function() {
        return { email: email.value }; //id.value
      });
    } else {
      this.setState(function() {
        return { password: password.value };
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    // console.log(event.target.email.value,event.target.password.value)

    this.props.onSubmit(email.value, password.value);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        email<input
          type="text"
          placeholder="email"
          autoComplete="off"
          id="email"
          value={this.state.email}
          onChange={this.handleChange}
        />
        password<input
          type="text"
          placeholder="password"
          autoComplete="off"
          id="password"
          value={this.state.password}
          onChange={this.handleChange}
        />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      err: {}
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(email, pwd) {
    if (!email || !pwd) {
      this.setState(() => {
        return {
          err: {
            error: true,
            msg: "Enter email and password"
          }
        };
      });
    } else {
      api
        .checkValidation(email, pwd)
        .then(response => {
          this.setState(() => {
            return {
              err: {
                error: false
              }
            };
          });
        })
        .catch(e => {
          this.setState(() => {
            return {
              err: {
                error: true,
                msg: e.response.data.message
              }
            };
          });
        });
    }
  }
  render() {
    return (
      <div>
        <UserInput onSubmit={this.handleSubmit} />
        {this.state.err.error && <p>{this.state.err.msg}</p>}
      </div>
    );
  }
}

module.exports = Login;
