var React = require("react");
var ReactRouter = require("react-router-dom");
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;
var Login = require("./Login.js");

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route render = {function(){
              return (
                <p>Not Found</p>
              )
            }} />
          </Switch>
        </div>
      </Router>
    );
  }
}

module.exports = App;
