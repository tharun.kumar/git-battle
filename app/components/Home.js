var React = require('react');
var {Link} = require('react-router-dom');


class Home extends React.Component {
  render () {
    return (
      <div className='home-container'>
        <h1>Github Battle: Battle your friends... and stuff.</h1>

        <Link className='button' to='/battle'>
          Battle
        </Link>
      </div>
    )
  }
}

module.exports = Home;


// class Clock extends React.Component {
//     constructor(props){
//       super(props);

//       this.state={
//         num : 1
//       }
//     }

//     componentDidMount(){
//       this.interval = window.setInterval(
//       function() {
//           this.setState(function(prevState) {
//             return {
//               num : prevState.num + 1
//             };
//           });
//         }.bind(this),1000)
//     }

//   render(){
//     return (
//       <div>
//         Seconds : {this.state.num}
//       </div>
//     )
//   }
// }
